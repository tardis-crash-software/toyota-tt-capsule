local PART={}
PART.ID = "toyota_handbrake"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/handbrake.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.85
PART.Sound = "Poogie/toyota/handbrake.wav"

TARDIS:AddPart(PART)
