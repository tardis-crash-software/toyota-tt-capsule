local PART={}
PART.ID = "toyota_lever3"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/lever3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "cem/toyota/lever3.wav"

TARDIS:AddPart(PART)