local PART={}
PART.ID = "toyota_crank2"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/crank2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.75
PART.Sound = "cem/toyota/crank2.wav"

TARDIS:AddPart(PART)