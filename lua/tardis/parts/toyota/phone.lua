local PART={}
PART.ID = "toyota_phone"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/phone.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "cem/toyota/phone.wav"

if SERVER then
	function PART:Use(ply)
		TARDIS:Control(self.Control, ply)

		local easter_egg_sound = self.interior.metadata.ToyotaEasterEggSound 

		if easter_egg_sound ~= nil and math.random(1, 5) == 5 then
			self.interior:Timer("toyota_phone_easteregg", 4, function()
				self:EmitSound(easter_egg_sound)
			end)
		end
	end
end

TARDIS:AddPart(PART)