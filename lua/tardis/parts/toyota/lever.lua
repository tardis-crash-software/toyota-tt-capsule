local PART={}
PART.ID = "toyota_lever"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "Poogie/toyota/red_lever.wav"

TARDIS:AddPart(PART)