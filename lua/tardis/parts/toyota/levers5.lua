local PART={}
PART.ID = "toyota_levers5"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/levers5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "cem/toyota/levers.wav"

TARDIS:AddPart(PART)