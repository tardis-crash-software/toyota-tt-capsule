local PART={}
PART.ID = "toyota_crank5"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/crank5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.6
PART.Sound = "cem/toyota/crank.wav"

TARDIS:AddPart(PART)