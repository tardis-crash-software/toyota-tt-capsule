local PART={}
PART.ID = "toyota_monitor1"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/monitor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "cem/toyota/monitor.wav"


function PART:Initialize()
	if SERVER then
		local on = (math.random(0, 1) == 1)
		self:SetData("toyota_monitor1_on", on, true)
	end

	local on = self:GetData("toyota_monitor1_on")

	if on then
		self:SetOn(true)
		self.posepos=1
		self:SetPoseParameter("switch", 1)
	end

	if CLIENT then return end

	if not on then
		self:SetCollide(false, true)
	end

	local hitbox = self.interior:GetPart("toyota_monitor1_hitbox")
	if IsValid(hitbox) then
		hitbox:SetCollide(not on, true)
	end
end

if SERVER then
	function PART:Use(ply)
		TARDIS:Control(self.Control, ply)

		if CLIENT then return end
		local hitbox = self.interior:GetPart("toyota_monitor1_hitbox")
		if not IsValid(hitbox) then return end

		self:SetCollide(false, true)
		hitbox:SetCollide(true, true)
	end
end

TARDIS:AddPart(PART)

local PART={}
PART.ID = "toyota_monitor1_hitbox"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = false
PART.Sound = "cem/toyota/monitor.wav"
PART.Name = "Toyota TARDIS monitor 1 hitbox"
PART.Model = "models/hunter/plates/plate025.mdl"

function PART:Initialize()
	self:SetColor(Color(255,255,255,0))

	if CLIENT then return end

	local on = self:GetData("toyota_monitor1_on")
	if on ~= nil then
		self:SetCollide(not on, true)
	end
end

if SERVER then
	function PART:Use(ply)
		TARDIS:Control(self.Control, ply)

		local monitor = self.interior:GetPart("toyota_monitor1")

		monitor:SetOn(true)
		monitor.posepos=1
		monitor:SetPoseParameter("switch", 1)

		if CLIENT then return end

		self:SetCollide(false, true)

		if IsValid(monitor) then
			monitor:SetCollide(true, true)
		end
	end
end

TARDIS:AddPart(PART)