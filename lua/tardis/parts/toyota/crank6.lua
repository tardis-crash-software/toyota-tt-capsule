local PART={}
PART.ID = "toyota_crank6"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/crank6.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.75
PART.Sound = "cem/toyota/crank.wav"

TARDIS:AddPart(PART)