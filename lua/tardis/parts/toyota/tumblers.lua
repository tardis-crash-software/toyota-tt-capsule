local PART={}
PART.ID = "toyota_tumblers"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/tumblers.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "cem/toyota/switch.wav"

TARDIS:AddPart(PART)