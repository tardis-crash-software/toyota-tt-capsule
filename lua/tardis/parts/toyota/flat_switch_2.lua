local PART={}
PART.ID = "toyota_flat_switch_2"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/flat_switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5
PART.Sound = "cem/toyota/switch.wav"

TARDIS:AddPart(PART)