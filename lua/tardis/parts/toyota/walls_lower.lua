local PART={}
PART.ID = "toyota_walls_lower"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/walls_lower.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

PART.ID = "toyota_walls_lower_roundels"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/walls_lower_roundels.mdl"

TARDIS:AddPart(PART)