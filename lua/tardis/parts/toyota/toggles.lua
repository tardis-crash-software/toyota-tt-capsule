local PART={}
PART.ID = "toyota_toggles"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/toggles.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "cem/toyota/toggles.wav"

TARDIS:AddPart(PART)