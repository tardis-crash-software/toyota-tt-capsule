local PART={}
PART.ID = "toyota_flippers"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/flippers.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.Sound = "cem/toyota/flippers.wav"

TARDIS:AddPart(PART)