TARDIS:AddControl({
	id = "toyota_toggle_moving_screen",
	tip_text = "Screen UI",
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		local on = not self:GetData("toyota_screen_ui_allowed")
		self:SetData("toyota_screen_ui_allowed", on)

		local monitor = self:GetPart("toyota_monitor1")
		if not IsValid(monitor) then return end

		self:SetScreensOn(on and not monitor:GetOn())
	end,
})

TARDIS:AddControl({
	id = "toyota_toggle_screens_delayed",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		if self:GetData("screens_on", false) then
			self:ToggleScreens()
		else
			self:Timer("toyota_toggle_screens", 0.5, function()
				self:ToggleScreens()
			end)
		end
	end,
})

function TARDIS:IsSonicInstalled()
	if SonicSD and file.Exists("autorun/sonicsd.lua", "LUA") then
		return true
	end
	return false
end

TARDIS:AddControl({
	id = "toyota_sonic_dispenser",
	int_func=function(self,ply)
		if TARDIS:IsSonicInstalled() then

			local interior_sonic_id = self.metadata.ToyotaSonicID

			local currentWeapon = ply:GetActiveWeapon()
			if IsValid(currentWeapon) and currentWeapon:GetClass() == "swep_sonicsd" then

				if currentWeapon:GetSonicID() ~= interior_sonic_id then
					currentWeapon:SetSonicID(interior_sonic_id)
					currentWeapon:CallHook("SonicChanged")
					TARDIS:Message(ply, "Sonic Screwdriver has been equipped.")
				end

				return
			end
			if IsValid(ply:GetWeapon("swep_sonicsd")) then
				TARDIS:Message(ply, "Sonic Screwdriver has been equipped.")
			else
				ply:Give("swep_sonicsd")
				TARDIS:Message(ply, "Sonic Screwdriver has been dispensed.")
			end
			ply:SelectWeapon("swep_sonicsd")

			local currentWeapon = ply:GetActiveWeapon()
			if currentWeapon:GetSonicID() ~= interior_sonic_id then
				currentWeapon:SetSonicID(interior_sonic_id)
				currentWeapon:CallHook("SonicChanged")
			end

		else
			TARDIS:ErrorMessage(ply, "You do not have the Sonic Screwdriver addon installed. Install it for this part to work.")
		end
	end,
	power_independent = true,
	serveronly = true,
	screen_button = false,
	tip_text = "Sonic Charger",
})