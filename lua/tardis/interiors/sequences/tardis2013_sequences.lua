--11th TARDIS interior - Control sequences (advanced mode)

local Seq = {
	ID = "smith_sequences",

	["toyota_keyboard"] = {
		Controls = {
			"toyota_sliders",
			"toyota_levers",
			"toyota_gears",
			"toyota_lever4",
			"toyota_cranks2",
			"toyota_lever",
			"toyota_lever2",
			"toyota_handbrake",
			"toyota_throttle"
		},
		OnFinish = function(self, ply, step, part)
			if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end,
		OnFail = function(self, ply, step, part)
			// fail feature
		end
	},

	["dtsmithtelepathic"] = {
		Controls = {
			"toyota_sliders",
			"toyota_levers",
			"toyota_gears",
			"toyota_lever4",
			"toyota_cranks2",
			"toyota_lever",
			"toyota_lever2",
			"toyota_handbrake",
			"toyota_throttle",
		},
		OnFinish = function(self, ply, step, part)
			if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end,
		OnFail = function(self, ply, step, part)
			// fail  feature
		end
	}
}

TARDIS:AddControlSequence(Seq)