local T={
	ID="toyota_base",
	Base="base",
	Name = "Toyota - Blue 2013",
	Hidden = true,
}

T.Interior={
	Model="models/cem/toyota/interior_2013.mdl",
	Fallback={
		pos=Vector(-300,0,100),
		ang=Angle(0,0,0)
	},
	ExitDistance=1200,
	LightOverride = {
		basebrightness = 0.0169,
		nopowerbrightness = 0.025
	},
	Light={
		-- colors are defined in templates
		pos=Vector(0,0,110),
		brightness=0.5
	},
	Lights={
		console_white = {
			-- colors are defined in templates
			pos=Vector(0,0,189.5),
			brightness=0.69
		},
		lower_light = {
			-- colors are defined in templates
			pos=Vector(0,0,-30),
			brightness=6
		},
	},// Idle Sounds
	IdleSound={
		{
			path="cem/toyota/interior_idle_loop.wav",
			volume=1
		}
	},
	ScreensEnabled = false,
	Screens = {
		{
			visgui_rows = 3,
			power_off_black = false,
		},
	},
	Seats={
		{
			pos=Vector(-145,-83,104),
			ang=Angle(0,-65,0)
		},
		{
			pos=Vector(9,-165,104),
			ang=Angle(0,0,0)
		},
		{
			pos=Vector(160,29,104),
			ang=Angle(0,100,0)
		},
		{
			pos=Vector(43,155,104),
			ang=Angle(0,170,0)
		}
	},
	Parts={
		console=false,
		toyota_console 				= {ang = Angle(0,90,0)},
		toyota_sonic_charger 		= {ang = Angle(0,90,0)},
		toyota_walls_upper			= {pos = Vector(0, 0, 0), ang = Angle(0,90,0)},
		toyota_walls_lower			= {pos = Vector(0, 0, 0), ang = Angle(0,90,0)},
		toyota_rotor_piston			= {},
		toyota_rotors 				= {},
		toyota_roundels 			= {},
		toyota_roundels2 			= {pos = Vector(0, 0, 0), ang = Angle(0,100,0),},
		toyota_roundels3 			= {pos = Vector(0, 0, 0), ang = Angle(0,-100,0),},
		toyota_roundelstone			= {},
		toyota_rails 				= {},
		toyota_glass				= {},
		toyota_console_details 		= {pos = Vector(0, 0, 0), ang = Angle(0,90,0)},
		toyota_console_side_details1= {pos = Vector(0, 0, 0), ang = Angle(0,-0.07,0)},
		toyota_console_side_details2= {pos = Vector(0, 0, 0), ang = Angle(0,198.97,0)},
		toyota_lights 				= {},
		toyota_neon_lights 			= {pos = Vector(0, 0, 0), ang = Angle(0,0,0)},
		toyota_lower_trim			= {},
		toyota_hull_struts			= {},
		toyota_gears				= {},
		toyota_key					= {},
		toyota_lights_catwalk_upper	= {},
		toyota_lights_floor		    = {},
		toyota_handbrake			= {},
		toyota_balls				= {},
		toyota_buttons				= {},
		toyota_button				= {},
		toyota_button2				= {pos = Vector(0,9.4,0), ang = Angle(0,0,0)},
		toyota_crank				= {},
		toyota_crank2				= {},
		toyota_crank3				= {},
		toyota_crank4				= {},
		toyota_crank5				= {},
		toyota_crank6				= {},
		toyota_cranks				= {},
		toyota_cranks2				= {pos = Vector(0, 0, 0), ang = Angle(0,199,0)},
		toyota_flippers				= {},
		toyota_lever				= {},
		toyota_lever2				= {pos = Vector(0,26.55,0), ang = Angle(0,0,0)},
		toyota_lever3				= {},
		toyota_lever4				= {pos = Vector(0, 0, 0), ang = Angle(0,199,0)},
		toyota_lever5				= {},
		toyota_levers				= false,
		toyota_levers1				= {pos = Vector(31.2772, -6.47828, 134.362), ang = Angle(0,0,0)},
		toyota_levers2				= {pos = Vector(31.2772, -3.22899, 134.362), ang = Angle(0,0,0)},
		toyota_levers3				= {pos = Vector(31.2772, 0, 134.362), ang = Angle(0,0,0)},
		toyota_levers4				= {pos = Vector(31.2772, 3.23634, 134.362), ang = Angle(0,0,0)},
		toyota_levers5				= {pos = Vector(31.2772, 6.47268, 134.362), ang = Angle(0,0,0)},
		toyota_spin1				= {pos = Vector(-48.304, 9.401, 129.791), ang = Angle(-2.666, -16.7, 3.02)},
		toyota_spin2				= {pos = Vector(-48.182, 4.707, 129.578), ang = Angle(1.539, -10.568, 3.746)},
		toyota_spin3				= {pos = Vector(-48.121, -0.009, 129.534), ang = Angle(1.539, -5.024, 3.746)},
		toyota_spin4				= {pos = Vector(-48.191, -4.644, 129.553), ang = Angle(1.539, -6.344, 3.746)},
		toyota_spin5				= {pos = Vector(-48.213, -9.453, 129.64), ang = Angle(0, -13.464, 0)},
		toyota_spin6				= {pos = Vector(-10.011, 45.859, 130.933), ang = Angle(0, -2.574, 0)},
		toyota_spin7				= {pos = Vector(-14.892, 46.776, 129.528), ang = Angle(-2.88, -7.877, 4.089)},
		toyota_spin8				= {pos = Vector(-33.016, 36.267, 129.614), ang = Angle(-4.679, -21.486, 4.023)},
		toyota_spin9				= {pos = Vector(-34.663, 31.617, 130.915), ang = Angle(1.556, 9.082, -1.378)},
		toyota_spin10				= {pos = Vector(33.5519, -30.4627, 130.518), ang = Angle(11.6955,-19.3464,-2)},
		toyota_spin11				= {pos = Vector(-39.865, -31.28, 129.931), ang = Angle(0, -4.62, 0)},
		toyota_spin12				= {pos = Vector(32.9344, -25.9602, 132.217), ang = Angle(0, -4.62, 0)},
		toyota_red_flick_cover		= {pos = Vector(46.8003, 20.3683, 130.056), ang = Angle(0,0,0)},
		toyota_red_flick_switch		= {pos = Vector(48.3763, 20.3791, 129.36), ang = Angle(0,0,0)},
		toyota_handle1				= {pos = Vector(-32.0253, -11.1286, 136.032), ang = Angle(33.87, 94.5, -24.402)},
		toyota_handle2				= {pos = Vector(-32.0253, 11.1286, 136.032), ang = Angle(0,0,0)},
		toyota_bouncy_lever			= {pos = Vector(37.6148, 12.5797, 134.562), ang = Angle(0,0,0)},
		toyota_ducks				= {pos = Vector(0,0,0), ang = Angle(0,0,0)},
		toyota_fiddle1				= {pos = Vector(-47.8258,20.3926,128.359), ang = Angle(0,0,0)},
		toyota_fiddle2				= {pos = Vector(-47.8258, 17.3307,128.359), ang = Angle(0,0,0)},
		toyota_phone				= {},
		toyota_rooms				= {},
		toyota_intdoors				= {},
		toyota_keyboard				= {},
		toyota_audio_system			= {},
		toyota_console_scanner		= {},
		toyota_sliders				= {},
		toyota_switch				= {},
		toyota_switch2				= {},
		toyota_flat_switch_1		= {pos = Vector(-10.1897,28.1115,137.23), ang = Angle(0,0,0)},
		toyota_flat_switch_2		= {pos = Vector(-11.3625,27.4343,137.23), ang = Angle(0,0,0)},
		toyota_flat_switch_3		= {pos = Vector(-12.5354,26.7572,137.23), ang = Angle(0,0,0)},
		toyota_flat_switch_4		= {pos = Vector(-16.7892,24.3012,137.23), ang = Angle(0,0,0)},
		toyota_flat_switch_5		= {pos = Vector(-17.9621,23.6241,137.23), ang = Angle(0,0,0)},
		toyota_flat_switch_6		= {pos = Vector(-19.135,22.9469,137.23), ang = Angle(0,0,0)},
		toyota_small_switch_1		= {pos = Vector(-43.5688,9.2562,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_2		= {pos = Vector(-43.5688,8.45203,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_3		= {pos = Vector(-43.5688,7.64787,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_4		= {pos = Vector(-43.5688,6.84371,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_5		= {pos = Vector(-43.5688,6.03954,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_6		= {pos = Vector(-43.5688,-2.63501,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_7		= {pos = Vector(-43.5688,5.23538,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_8		= {pos = Vector(-43.5688,4.43121,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_9		= {pos = Vector(-43.5688,3.62705,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_10		= {pos = Vector(-43.5688,2.82289,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_11		= {pos = Vector(-43.5688,-3.43917,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_12		= {pos = Vector(-43.5688,-4.24334,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_13		= {pos = Vector(-43.5688,-5.0475,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_14		= {pos = Vector(-43.5688,-5.85166,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_15		= {pos = Vector(-43.5688,-6.65583,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_16		= {pos = Vector(-43.5688,-7.45999,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_17		= {pos = Vector(-43.5688,-8.26416,129.997), ang = Angle(0,0,0)},
		toyota_small_switch_18		= {pos = Vector(-43.5688,-9.06832,129.997), ang = Angle(0,0,0)},
		toyota_telepathic			= {},
		toyota_throttle				= {},
		toyota_toggles				= {},
		toyota_toggles2				= {},
		toyota_tumblers				= {},
		toyota_trim					= {},
		toyota_hitbox1				= {pos = Vector(6.27, -51.833, 127.407), ang = Angle(17.274, -45.918, 4.22), },
	},
	PartTips = {
		toyota_throttle 		= {pos = Vector(44.891, 14.683, 132.679),	right = false,	down = true, },
		toyota_handbrake 		= {pos = Vector(46.248, -16.804, 131.436),	right = true,	down = true, },
		toyota_lever3 			= {pos = Vector(103.41, 121.655, 130.044),	right = true,	down = false, },
		toyota_audio_system		= {pos = Vector(25.339, -27.011, 134.427),	right = true,	down = false, },
		toyota_telepathic		= {pos = Vector(19.919, 35.908, 130.754),	right = true,	down = false, },
		toyota_console_scanner	= {pos = Vector(-24.497, 34.429, 130.864),	right = false,	down = false, },
		toyota_keyboard			= {pos = Vector(20.96, -43.773, 128.294),	right = true,	down = false, },
		toyota_crank4			= {pos = Vector(-34.917, -29.135, 132.425),	right = true,	down = true, },
		toyota_toggles			= {pos = Vector(39.523, 0.016, 133.705),	right = true,	down = false, },
		toyota_crank2			= {pos = Vector(-9.156, -47.859, 130.481),	right = true,	down = true, },
		toyota_crank3			= {pos = Vector(-6.948, -30.268, 137.647),	right = false,	down = false, },
		toyota_lever4 			= {pos = Vector(-59.115, -151.548, 126.17),	right = true,	down = false, },
		toyota_buttons			= {pos = Vector(10.193, -49.502, 128.582),	right = true,	down = true,  },
		toyota_switch2			= {pos = Vector(-35.645, 12.629, 135.094),	right = true,	down = true,  },
		toyota_switch			= {pos = Vector(-45.646, -17.836, 130.267),	right = true,	down = true,  },
		toyota_lever5			= {pos = Vector(-36.787, -13.688, 134.195),	right = true,	down = false, },
		toyota_crank5			= {pos = Vector(-24.084, 21.564, 136.681),	right = false,	down = false, },
		toyota_crank6			= {pos = Vector(-6.901, 31.399, 136.842),	right = false,	down = false, },
		toyota_crank			= {pos = Vector(30.237, -28.123, 132.312),	right = true,	down = true,  },
		toyota_spin12			= {pos = Vector(33.531, -25.783, 132.247),	right = true,	down = false,  },
		toyota_hitbox1			= {pos = Vector(6.577, -51.692, 128.632),	right = false,	down = true,  },
		toyota_tumblers			= {pos = Vector(35.573, -11.599, 134.216),	right = true,	down = false, },
		toyota_button2			= {pos = Vector(39.263, 4.64, 132.233),		right = true,	down = false, },
		toyota_button			= {pos = Vector(39.263, -4.64, 132.233),	right = false,	down = false, },
		toyota_handle2			= {pos = Vector(-33.066, 10.939, 137.381),	right = true,	down = false, },
		toyota_handle1			= {pos = Vector(-32.921, -10.81, 137.31),	right = false,	down = false, },
		toyota_red_flick_switch	= {pos = Vector(47.918, 20.722, 129.642),	right = true,	down = true, },
		toyota_bouncy_lever		= {pos = Vector(37.33, 12.944, 133.946),	right = true,	down = false, },
		toyota_lever			= {pos = Vector(-44.239, -12.951, 131.572),	right = true,	down = false,	text = "Protocol activation \n lever", },
		toyota_lever2			= {pos = Vector(-44.051, 14.102, 131.376),	right = false,	down = false,	text = "Protocol activation \n lever", },
	},
	CustomTips = {
		{pos = Vector(-36.982, -151.084, 128.849),	down = true, right = false,	part = "toyota_audio_system", },
		{pos = Vector(-106.793, -119.792, 123.778),	down = true, right = true,	part = "toyota_audio_system", },
		{pos = Vector(88.949, 132.435, 125.284),	down = true, right = true,	part = "toyota_audio_system", },
		{pos = Vector(139.173, 77.856, 125.267),	down = true, right = true,	part = "toyota_audio_system", },
	},
	Controls = {
		toyota_throttle 		= "teleport",
		toyota_handbrake 		= "handbrake",
		toyota_lever3			= "engine_release",
		toyota_audio_system		= "music",
		toyota_crank 			= "door",
		toyota_telepathic 		= "destination",
		toyota_console_scanner	= "thirdperson",
		toyota_balls 			= "thirdperson",
		toyota_keyboard 		= "coordinates",
		toyota_crank4			= "power",
		toyota_crank2 			= "repair",
		toyota_crank3 			= "redecorate",
		toyota_lever4			= "physlock",
		toyota_buttons			= "isomorphic",
		toyota_switch2 			= "spin_toggle",
		toyota_crank6			= "spin_switch",
		toyota_crank5			= "cloak",
		toyota_tumblers			= "vortex_flight",
		toyota_button2			= "hads",
		toyota_spin12			= "doorlock",
		toyota_handle1			= "flight",
		toyota_handle2			= "float",
		toyota_red_flick_switch	= "engine_release",
		toyota_bouncy_lever		= "fastreturn",

		toyota_button			= "toyota_toggle_moving_screen",
		toyota_monitor1			= "toyota_toggle_screens_delayed",
		toyota_monitor1_hitbox	= "toyota_toggle_screens_delayed",
		toyota_sonic_charger	= "sonic_dispenser",
	},
	CustomHooks = {
		screens_condition = {
			"CanEnableScreens",
			function(self)
				local monitor = self:GetPart("toyota_monitor1")
				if not IsValid(monitor) then
					return false
				end
				if monitor:GetOn() then
					return false
				end
				if not self:GetData("toyota_screen_ui_allowed") then
					return false
				end
			end,
		},
	},
	// this is the sound section you can modify sounds here with the path of which sounds you want
	Sounds={
		Teleport={
			demat="Poogie/toyota/dematint.wav",
			mat="cem/toyota/mat.wav",
			fullflight = "cem/toyota/full.wav",
			demat_fail = "Poogie/toyota/demat_fail.wav",
		},
		FlightLoop="cem/toyota/flight_loop.wav",
		Power = {
            On = "cem/toyota/powerup.wav",
            Off = "cem/toyota/powerdown.wav"
		},
	},
	TipSettings = {
		view_range_min = 40,
		view_range_max = 75,
		--style = "smith",
	},

}
T.Exterior={
	ScannerOffset = Vector(25,0,50),
	Sounds={
		FlightLoop="cem/toyota/flight_loopext.wav",
		RepairFinish="cem/toyota/repairfinish.wav",
		Lock="cem/toyota/lock.wav",
		Cloak = "cem/toyota/cloak.wav",
		CloakOff = "cem/toyota/uncloak.wav",
	},
	Teleport={
		SequenceSpeed=0.63,
		SequenceSpeedFast=1,
	},
}

T.Interior.TextureSets = {
	initial_roundel_trim = {
		-- prefix is defined in templates
		{"toyota_lights_lower_roundels", 0, "roundel_trim"},
		{"toyota_lights_lower_roundels", 1, "roundel_trim_moving"},
		{"toyota_lights_lower_roundels", 2, "roundel_trim_warning"},
		{"toyota_lights_lower_roundels", 3, "roundel_trim_off"},
	},
	poweron = {
		prefix = "models/cem/toyota_smith/",
		{"toyota_console", 1, "console"},
		{"self", 3, "console"},
		{"self", 4, "rotorbottcap"},
		{"toyota_console", 0, "chairs"},
		{"toyota_console", 2, "lightsconsole"},

		{"toyota_books", 4, "lightsconsole"},

		{"toyota_console", 3, 		"detailsm"},
		{"toyota_console_details", 1, 		"detailsm"},
		{"toyota_console_side_details1", 0, 		"detailsm"},
		{"toyota_console_side_details2", 0, 		"detailsm"},
		{"toyota_upperconsolelights", 1, 		"lightsconsole"},
		{"toyota_monitor_ring", 2, 	"detailsm"},
		{"toyota_crank", 0, 		"detailsm"},
		{"toyota_crank2", 0, 		"detailsm"},
		{"toyota_crank3", 0, 		"detailsm"},
		{"toyota_crank5", 1, 		"detailsm"},
		{"toyota_crank6", 1, 		"detailsm"},
		{"toyota_cranks", 0, 		"detailsm"},
		{"toyota_cranks2", 0, 		"detailsm"},
		{"toyota_gears", 1, 		"detailsm"},
		{"toyota_handbrake", 0, 	"detailsm"},
		{"toyota_lever3", 1, 		"detailsm"},
		{"toyota_lever4", 1, 		"detailsm"},
		{"toyota_levers", 1, 		"detailsm"},
		{"toyota_levers1", 1, 		"detailsm"},
		{"toyota_levers2", 1, 		"detailsm"},
		{"toyota_levers3", 1, 		"detailsm"},
		{"toyota_levers4", 1, 		"detailsm"},
		{"toyota_levers5", 1, 		"detailsm"},
		{"toyota_monitor1", 1, 		"detailsm"},
		{"toyota_monitor1", 2, 		"monitorm"},
		{"toyota_monitor2", 1, 		"detailsm"},
		{"toyota_monitor2", 2, 		"monitorm"},
		{"toyota_red_flick_switch",	"newswitches"},
		{"toyota_sonic_charger", 1, "detailsm"},
		{"toyota_switch", 0, 		"detailsm"},
		{"toyota_throttle", 0, 		"detailsm"},
		{"toyota_throttle_2014", 0, "detailsm"},
		{"toyota_toggles2", 0, 		"detailsm"},
		{"toyota_tumblers", 1, 		"detailsm"},

		{"toyota_small_switch_1", 	"newswitches"},
		{"toyota_small_switch_2", 	"newswitches"},
		{"toyota_small_switch_3", 	"newswitches"},
		{"toyota_small_switch_4", 	"newswitches"},
		{"toyota_small_switch_5", 	"newswitches"},
		{"toyota_small_switch_6", 	"newswitches"},
		{"toyota_small_switch_7", 	"newswitches"},
		{"toyota_small_switch_8", 	"newswitches"},
		{"toyota_small_switch_9", 	"newswitches"},
		{"toyota_small_switch_10", 	"newswitches"},
		{"toyota_small_switch_11", 	"newswitches"},
		{"toyota_small_switch_12", 	"newswitches"},
		{"toyota_small_switch_13", 	"newswitches"},
		{"toyota_small_switch_14", 	"newswitches"},
		{"toyota_small_switch_15", 	"newswitches"},
		{"toyota_small_switch_16", 	"newswitches"},
		{"toyota_small_switch_17", 	"newswitches"},
		{"toyota_small_switch_18", 	"newswitches"},
		{"toyota_flat_switch_1", 	"newswitches"},
		{"toyota_flat_switch_2", 	"newswitches"},
		{"toyota_flat_switch_3", 	"newswitches"},
		{"toyota_flat_switch_4", 	"newswitches"},
		{"toyota_flat_switch_5", 	"newswitches"},
		{"toyota_flat_switch_6", 	"newswitches"},

		{"toyota_lights", 0, "neonlights"},
		{"toyota_lights", 1, "lightsconsole"},
		{"toyota_lights", 2, "red"},
		{"toyota_lights", 3, "purple"},

		{"toyota_rotors", 0, "rotorlights"},

		{"toyota_monitor_ring", 0, "rotor"},

		{ "toyota_console_scanner", 0, "screen" },
	},
	poweroff = {
		base = "poweron",
		prefix = "models/cem/toyota_smith/off/",
	},


	normal = {
		prefix = "models/cem/toyota_smith/",
		[1] = {"self", 1, "ceilingl"},
		[2] = {"toyota_lights_catwalk_upper", "catwalklights"},
		[3] = {"toyota_lights_floor", "catwalklightsfloor"},
		[4] = {"toyota_roundelstone", "roundelstone"},
		[5] = {"toyota_telepathic", "telepathic"},
		[6] = {"toyota_lights_catwalk_lower", "catwalklightslow"},
		[7] = {"toyota_lights_lower_roundels", 0},
	},
	off = {
		base = "normal",
		prefix = "models/cem/toyota_smith/off/",
		[7] = {"toyota_lights_lower_roundels", 3},
	},
	warning = {
		base = "normal",
		prefix = "models/cem/toyota_smith/warning/",
		[7] = {"toyota_lights_lower_roundels", 2},
	},
	flight  = {
		base = "normal",
		prefix = "models/cem/toyota_smith/flight/",
		[7] = {"toyota_lights_lower_roundels", 1},
	},
	warning_flight = {
		base = "normal",
		prefix = "models/cem/toyota_smith/warning_flight/",
		[7] = {"toyota_lights_lower_roundels", 2},
	},


	poweron_specific = {
		{"toyota_walls_lower", "walls_lower"},
		{"toyota_walls_lower_roundels", "walls_lower_lit"},
		{"toyota_neon_lights", 0, "neon_mid"}, -- time rotor
		{"toyota_neon_lights", 1, "neon"},
		{"toyota_neon_lights", 2, "neon_outer"},
		{"toyota_floor", 0, "floor"},
		{"toyota_floor", 1, "mainfloor"},
		{"toyota_floor", 2, "lowerfloor"},
		{"toyota_floor", 3, "stairs"},

		{"toyota_floor_dotd_step", 0, "floor"},
		{"toyota_floor_dotd_step", 1, "mainfloor"},

		{"toyota_glass", 0, "glass"},
		{"toyota_glass", 2, "consoleglass"},
		{"toyota_glass", 3, "glass_green"},

		{"toyota_rotor_piston", 0, "caps"},
		{"toyota_rotor_piston", 1, "rotorglass"},
		{"toyota_rotor_piston", 2, "caps"},

		{"self", 5, "rotor"},
		{"self", 6, "rotorm"},
	},
	poweroff_specific = {
		base = "poweron_specific",
	},

	screenon = {
		{ "toyota_monitor1", 3, "monitor_screen" },
		{ "toyota_monitor2", 3, "monitor_screen" },
	},
	-- screenvortex is defined in vortex template
	screenoff = {
		{ "toyota_monitor1", 3, "black" },
		{ "toyota_monitor2", 3, "black" },
	},
	screenwarning = {
		{ "toyota_monitor1", 3, "black" },
		{ "toyota_monitor2", 3, "black" },
	},
}

local TEXTURE_UPDATE_DATA_IDS = {
	["power-state"] = true,
	["health-warning"] = true,
	["teleport"] = true,
	["vortex"] = true,
	["flight"] = true,
}

T.CustomHooks = {
	screen_textures = {
		exthooks = {
			["PowerToggled"] = true,
			["HealthWarningToggled"] = true,
			["StopDemat"] = true,
			["MatStart"] = true,
		},
		inthooks = {
			["PostInitialize"] = true,
		},
		func = function(ext, int)
			local power = ext:GetData("power-state")
			local warning = ext:GetData("health-warning", false)
			local vortex = ext:GetData("vortex")
			local vortex_flight = not ext:GetData("demat-fast")

			if not power then
				int:ApplyTextureSet("screenoff")
			elseif warning then
				int:ApplyTextureSet("screenwarning")
			elseif vortex and vortex_flight then
				int:ApplyTextureSet("screenvortex")
			else
				int:ApplyTextureSet("screenon")
			end
		end,
	},
	initial_textures = {
		inthooks = {
			["PostInitialize"] = true,
		},
		func = function(ext, int)
			int:ApplyTextureSet("poweron")
			int:ApplyTextureSet("poweron_specific")
			int:ApplyTextureSet("normal")
			int:ApplyTextureSet("initial_roundel_trim")
		end,
	},
	monitors_rotation = {
		inthooks = {
			["PostInitialize"] = true,
		},
		func = function(ext, int)
			local m1 = int:GetPart("toyota_monitor1")
			local m2 = int:GetPart("toyota_monitor2")

			if not IsValid(m1) or not IsValid(m2) then return end

			local offset = 180 + math.random(-54, 54)
			m2:SetAngles(m1:GetAngles() + Angle(0, offset, 0))
		end,
	},
	power_textures = {
		exthooks = {
			["PowerToggled"] = true,
		},
		func = function(ext, int, on)
			int:ApplyTextureSet(on and "poweron" or "poweroff")
			int:ApplyTextureSet((on and "poweron" or "poweroff") .. "_specific")
		end,
	},
	travel_textures = {
		exthooks = {
			["DataChanged"] = true,
		},
		func = function(ext, int, data_id, data_value)
			if not TEXTURE_UPDATE_DATA_IDS[data_id] then return end

			prefix = "models/cem/toyota_smith/"

			local power = ext:GetData("power-state")
			local warning = ext:GetData("health-warning")
			local teleport = ext:GetData("teleport")
			local flight = ext:GetData("flight")
			local vortex = ext:GetData("vortex")

			if not power then
				int:ApplyTextureSet("off")
			else
				if flight or teleport or vortex then
					int:ApplyTextureSet(warning and "warning_flight" or "flight")
				else
					int:ApplyTextureSet(warning and "warning" or "normal")
				end
			end
		end,
	},
	doorframe_light = {
		exthooks = {
			["PowerToggled"] = true,
		},
		inthooks = {
			["PostInitialize"] = true,
		},
		func = function(ext, int)
			local power = ext:GetData("power-state")
			if power then
				int:ApplyTextureSet("doorframe_on")
			else
				int:ApplyTextureSet("doorframe_off")
			end
		end,
	},
}

TARDIS:AddInterior(T)