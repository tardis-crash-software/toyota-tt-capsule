local function select_floor_type(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "floor_type", ply)
end

local function select_int_color(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "interior_color", ply)
end

local function select_exterior(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "exterior", ply)
end

local function select_vortex(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "vortex", ply)
end

local function select_console(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "console", ply)
end

local function select_monitors(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "monitors", ply)
end

local function select_throttle_sound(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "throttle_sound", ply)
end

local function select_mat_sound(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "mat_sound", ply)
end

local function select_demat_sound(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "demat_sound", ply)
end

local function select_bookshelves(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "bookshelves", ply)
end

local function select_phone_port(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "phone_port", ply)
end

local function select_lower_roundels(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "lower_roundels", ply)
end

local function select_green_rotor(id, ply, ent)
	return TARDIS:GetCustomSetting(id, "green_rotor", ply)
end


local T = {
	ID = "toyota_custom",
	Name = "2017-2012 Custom Toyota",
	Base = "toyota_base",
}

T.CustomSettings = {
	floor_type = {
		text = "Floor",
		value_type = "list",
		value = "S8+",
		options = {
			["S7"]		= "2013 (Series 7)",
			["dotd"]	= "2013 (Day of the Doctor)",
			["totd"]	= "2013 (Time of the Doctor)",
			["S8+"]		= "2014+",
		}
	},
	interior_color = {
		text = "Interior color",
		value_type = "list",
		value = "blue",
		options = {
			["blue"] = "Blue (2012-2013)",
			["orange"] = "Orange (2014-2015)",
			["orange_b"] = "Orange bright (2017)",
			["blue_b"] = "Blue bright",
		}
	},
	exterior = {
		text = "Exterior",
		value_type = "list",
		value = "2014_F",
		options = {
			["2013_E"] = "2012-2013 E box",
			["2014_F"] = "2013-2015 F box",
			["2017_E"] = "2017 E box",
			["2017_F"] = "2017 F box",
            ["2016_H"] = "2016 TT Capsule",
		}
	},
	vortex = {
		text = "Vortex",
		value_type = "list",
		value = "2013B",
		options = {
			["2013"] = "2013",
			["2014"] = "2014",
			["2013B"] = "2013B",
		}
	},
	console = {
		text = "Console",
		value_type = "list",
		value = "12th",
		options = {
			["snowmen"] = "Snowmen",
			["s7"] = "Series 7",
			["dotd"] = "DotD (with stasis cube)",
			["dotd_empty"] = "DotD (no stasis cube)",
			["11th"] = "S8 & TotD (11th sonic dispenser)",
			["12th"] = "S9+ (12th sonic dispenser)",
		}
	},
	monitors = {
		text = "Monitors",
		value_type = "list",
		value = "2013",
		options = {
			["2012"] = "2012",
			["2013"] = "2013-2014",
			["2015"] = "2015-2017",
		}
	},
	throttle_sound = {
		text = "Throttle sound",
		value_type = "list",
		value = "2013",
		options = {
			["2013"] = "2012-2013",
			["2014"] = "2014",
			["2015"] = "2015",
			["2017"] = "2017",

		}
	},
	demat_sound = {
		text = "Take off sound",
		value_type = "list",
		value = "2013",
		options = {
			["2012"] = "2012(Snowmen)",
			["2013"] = "2013(NOTD)",
			["2014"] = "2014",
			["2015"] = "2015",
			["2016"] = "2016",
			["2017"] = "2017(TUAT)",
			["Default"] = "Default",

		}
	},
	mat_sound = {
		text = "Landing sound",
		value_type = "list",
		value = "2013",
		options = {
			["2012"] = "2012",
			["2013"] = "2013",
			["2017"] = "2017",
			["Default"] = "Default",

		}
	},
	bookshelves = {
		text = "Bookshelves",
		category = "Details",
		value_type = "bool",
		value = true,
	},
	phone_port = {
		text = "Phone port",
		category = "Details",
		value_type = "bool",
		value = true,
	},
	lower_roundels = {
		text = "Lower roundels",
		category = "Details",
		value_type = "bool",
		value = true,
	},
	green_rotor = {
		text = "TotD green rotor",
		category = "Details",
		value_type = "bool",
		value = false,
	},
}

T.Templates = {
--------------------------------------------------------------------------------
-- Color
--------------------------------------------------------------------------------
	toyota_interior_blue = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_int_color(id, ply, ent)
			return (selected == "blue")
		end,
	},
	toyota_interior_orange = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_int_color(id, ply, ent)
			return (selected == "orange")
		end,
	},
	toyota_interior_orange_bright = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_int_color(id, ply, ent)
			return (selected == "orange_b")
		end,
	},
	toyota_interior_blue_bright = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_int_color(id, ply, ent)
			return (selected == "blue_b")
		end,
	},

--------------------------------------------------------------------------------
-- Floor type
--------------------------------------------------------------------------------

	toyota_floor_s7 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_floor_type(id, ply, ent)
			return (selected == "S7")
		end,
	},
	toyota_floor_totd = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_floor_type(id, ply, ent)
			return (selected == "totd")
		end,
	},
	toyota_floor_dotd_step = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_floor_type(id, ply, ent)
			return (selected == "dotd")
		end,
	},
	toyota_floor_2014 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_floor_type(id, ply, ent)
			return (selected == "S8+")
		end,
	},

--------------------------------------------------------------------------------
-- Monitors
--------------------------------------------------------------------------------
	toyota_monitors_2012 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_monitors(id, ply, ent)
			return (selected == "2012")
		end,
	},
	toyota_monitors_2013 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_monitors(id, ply, ent)
			return (selected == "2013")
		end,
	},
	toyota_monitors_2015 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_monitors(id, ply, ent)
			return (selected == "2015")
		end,
	},

--------------------------------------------------------------------------------
-- Vortex
--------------------------------------------------------------------------------
	toyota_vortex_2013 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_vortex(id, ply, ent)
			return (selected == "2013")
		end,
	},
	toyota_vortex_2014 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_vortex(id, ply, ent)
			return (selected == "2014")
		end,
	},
	toyota_vortex_2013B = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_vortex(id, ply, ent)
			return (selected == "2013B")
		end,
	},

--------------------------------------------------------------------------------
// Throttle sound
--------------------------------------------------------------------------------
	toyota_throttle_sound_2014 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_throttle_sound(id, ply, ent)
			return (selected == "2014")
		end,
	},
	toyota_throttle_sound_2015 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_throttle_sound(id, ply, ent)
			return (selected == "2015")
		end,
	},
	toyota_throttle_sound_2017 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_throttle_sound(id, ply, ent)
			return (selected == "2017")
		end,
	},

--------------------------------------------------------------------------------
// Console
--------------------------------------------------------------------------------
	toyota_console_snowmen = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_console(id, ply, ent)
			return (selected == "snowmen")
		end,
	},
	toyota_console_s7 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_console(id, ply, ent)
			return (selected == "s7")
		end,
	},
	toyota_console_dotd_holder_empty = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_console(id, ply, ent)
			return (selected == "dotd" or selected == "dotd_empty")
		end,
	},
	toyota_console_11th_sonic_dispenser = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_console(id, ply, ent)
			local file_ok = file.Exists("models/doctorwho1200/sonics/11thdoctor/3rdpersonsonic.mdl", "GAME")
			return file_ok and (selected == "11th")
		end,
	},
	toyota_console_12th_sonic_dispenser = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_console(id, ply, ent)
			local file_ok = file.Exists("models/doctorwho1200/sonics/12thdoctor/3rdpersonsonic.mdl", "GAME")
			return file_ok and (selected == "12th")
		end,
	},

--------------------------------------------------------------------------------
// Exterior model type
--------------------------------------------------------------------------------
	toyota_e_box_base = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_exterior(id, ply, ent)
			return (selected == "2013_E" or selected == "2017_E")
		end,
	},
	toyota_f_box_base = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_exterior(id, ply, ent)
			return (selected == "2014_F" or selected == "2017_F")
		end,
	},
        toyota_h_capsule_base = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_exterior(id, ply, ent)
			return (selected == "2016_H" or selected == "2017_H")
		end,
	},

--------------------------------------------------------------------------------
// Exterior model textures
--------------------------------------------------------------------------------
	toyota_e_box_2013 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_exterior(id, ply, ent)
			return (selected == "2013_E")
		end,
	},
	toyota_f_box_2014 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_exterior(id, ply, ent)
			return (selected == "2014_F")
		end,
	},
	toyota_e_box_2017 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_exterior(id, ply, ent)
			return (selected == "2017_E")
		end,
	},
	toyota_f_box_2017 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_exterior(id, ply, ent)
			return (selected == "2017_F")
		end,
	},
        toyota_h_capsule_2013 = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_exterior(id, ply, ent)
			return (selected == "2016_H")
		end,
	},

--------------------------------------------------------------------------------
// Separate features
--------------------------------------------------------------------------------
	toyota_bookshelves = {
		override = true,
		condition = function(id, ply, ent)
			return select_bookshelves(id, ply, ent)
		end,
	},

	toyota_phone_port = {
		override = true,
		condition = function(id, ply, ent)
			return select_phone_port(id, ply, ent)
		end,
	},

	toyota_lower_roundels = {
		override = true,
		condition = function(id, ply, ent)
			return select_lower_roundels(id, ply, ent)
		end,
	},

	toyota_totd_green_rotor = {
		override = true,
		condition = function(id, ply, ent)
			return select_green_rotor(id, ply, ent)
		end,
	},

	toyota_stasis_cube = {
		override = true,
		condition = function(id, ply, ent)
			local selected = select_console(id, ply, ent)
			return (selected == "dotd")
		end,
	},
----------------------------
// Demat sounds
----------------------------
toyota_demat_2012_snowmen = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_demat_sound(id, ply, ent)
		return (selected == "2012")
	end,
},

toyota_demat_2013_notd = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_demat_sound(id, ply, ent)
		return (selected == "2013")
	end,
},

toyota_demat_2014 = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_demat_sound(id, ply, ent)
		return (selected == "2014")
	end,
},

toyota_demat_2015 = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_demat_sound(id, ply, ent)
		return (selected == "2015")
	end,
},

toyota_demat_2017_F = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_demat_sound(id, ply, ent)
		return (selected == "2016")
	end,
},

toyota_demat_2017 = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_demat_sound(id, ply, ent)
		return (selected == "2017")
	end,
},

toyota_demat_stock = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_demat_sound(id, ply, ent)
		return (selected == "Default")
	end,
},

--------------------
// Mat sounds
--------------------
toyota_mat_2012_snowmen = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_mat_sound(id, ply, ent)
		return (selected == "2012")
	end,
},

toyota_mat_S7 = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_mat_sound(id, ply, ent)
		return (selected == "2013")
	end,
},

toyota_mat_2017_TUAT = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_mat_sound(id, ply, ent)
		return (selected == "2017")
	end,
},

toyota_mat_2014_stock = {
	override = true,
	condition = function(id, ply, ent)
		local selected = select_mat_sound(id, ply, ent)
		return (selected == "Default")
	end,
},

}

TARDIS:AddInterior(T)
