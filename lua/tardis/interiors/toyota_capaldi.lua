local YES = {override = true}

TARDIS:AddInterior({
	ID = "toyota_2015",
	Base = "toyota_base",
	Name = "2014-2017 TARDIS",
	Versions = {
		main = { id = "toyota_2015", },
		randomize = true,
		randomize_custom = false,
		other = {
			{
				id = "toyota_2014",
				name = "2014 (S8)"
			},
                        {
				id = "toyota_2017_T",
				name = "2017 (S9, TT Capsule)"
			},
                        {
			},
			{
				id = "toyota_2015",
				name = "2015 (S9)"
			},
			{
				id = "toyota_2017_E",
				name = "2017 (S10, E box)"
			},
			{
				id = "toyota_2017_F",
				name = "2017 (S10, F box)"
			},
		},
	},

	Templates = {
		toyota_f_box_base = YES,
		toyota_f_box_2014 = YES,
		toyota_vortex_2014 = YES,

		toyota_interior_orange = YES,
		toyota_monitors_2015 = YES,
		toyota_floor_2014 = YES,
		toyota_console_12th_sonic_dispenser = {
			override = true,
			condition = function(id, ply, ent)
				return file.Exists("models/doctorwho1200/sonics/12thdoctor/3rdpersonsonic.mdl", "GAME")
			end,
		},

		toyota_bookshelves = YES,
		toyota_phone_port = YES,
		toyota_lower_roundels = YES,
		toyota_throttle_sound_2015 = YES,
		toyota_demat_2015 = YES,
		toyota_demat_damaged_HORS = YES,
	}
})

TARDIS:AddInterior({
	ID = "toyota_2014",
	Base = "toyota_base",
	Name = "Toyota - Orange 2014",
	IsVersionOf = "toyota_2015",
	Templates = {
		toyota_f_box_base = YES,
		toyota_f_box_2014 = YES,
		toyota_vortex_2014 = YES,

		toyota_interior_orange = YES,
		toyota_monitors_2013 = YES,
		toyota_floor_2014 = YES,
		toyota_console_11th_sonic_dispenser = {
			override = true,
			condition = function(id, ply, ent)
				return file.Exists("models/doctorwho1200/sonics/11thdoctor/3rdpersonsonic.mdl", "GAME")
			end,
		},

		toyota_bookshelves = YES,
		toyota_throttle_sound_2014 = YES,
		toyota_demat_2014 = YES,
	},
})

TARDIS:AddInterior({
	ID="toyota_2017_E",
	Base="toyota_base",
	Name = "Toyota - Orange 2017 (E box)",
	IsVersionOf = "toyota_2015",
	Templates = {
		toyota_e_box_base = YES,
		toyota_e_box_2017 = YES,
		toyota_vortex_2014 = YES,

		toyota_interior_orange_bright = YES,
		toyota_monitors_2015 = YES,
		toyota_floor_2014 = YES,
		toyota_console_12th_sonic_dispenser = {
			override = true,
			condition = function(id, ply, ent)
				return file.Exists("models/doctorwho1200/sonics/12thdoctor/3rdpersonsonic.mdl", "GAME")
			end,
		},

		toyota_bookshelves = YES,
		toyota_phone_port = YES,
		toyota_lower_roundels = YES,
		toyota_throttle_sound_2017 = YES,
		toyota_demat_2017 = YES,
		toyota_mat_2017_TUAT = YES
	},
})

TARDIS:AddInterior({
	ID="toyota_2017_F",
	Base="toyota_base",
	Name = "Toyota - Orange 2017 (F box)",
	IsVersionOf = "toyota_2015",
	Templates = {
		toyota_f_box_base = YES,
		toyota_f_box_2017 = YES,
		toyota_vortex_2014 = YES,

		toyota_interior_orange_bright = YES,
		toyota_monitors_2015 = YES,
		toyota_floor_2014 = YES,
		toyota_console_12th_sonic_dispenser = {
			override = true,
			condition = function(id, ply, ent)
				return file.Exists("models/doctorwho1200/sonics/12thdoctor/3rdpersonsonic.mdl", "GAME")
			end,
		},

		toyota_bookshelves = YES,
		toyota_phone_port = YES,
		toyota_lower_roundels = YES,
		toyota_throttle_sound_2015 = YES,
		toyota_demat_2017_F = YES,
	},
})

TARDIS:AddInterior({
	ID="toyota_2017_H",
	Base="toyota_base",
	Name = "Toyota - Capaldi TT Capsule",
	IsVersionOf = "toyota_2015",
	Templates = {
		toyota_h_capsule_base = YES,
		toyota_h_capsule_2013 = YES,
		toyota_vortex_2014 = YES,

		toyota_interior_orange_bright = YES,
		toyota_monitors_2015 = YES,
		toyota_floor_2014 = YES,
		toyota_console_12th_sonic_dispenser = {
			override = true,
			condition = function(id, ply, ent)
				return file.Exists("models/doctorwho1200/sonics/12thdoctor/3rdpersonsonic.mdl", "GAME")
			end,
		},

		toyota_bookshelves = YES,
		toyota_phone_port = YES,
		toyota_lower_roundels = YES,
		toyota_throttle_sound_2017 = YES,
		toyota_demat_2017 = YES,
		toyota_mat_2017_TUAT = YES
	},
})

TARDIS:AddInterior({
	ID="toyota_2017_T",
	Base="toyota_base",
	Name = "Toyota - Capaldi TT Capsule",
	IsVersionOf = "toyota_2015",
	Templates = {
		toyota_h_capsule_base = YES,
		toyota_h_capsule_2013 = YES,
		toyota_vortex_2014 = YES,

		toyota_interior_orange = YES,
		toyota_monitors_2015 = YES,
		toyota_floor_2014 = YES,
		toyota_console_12th_sonic_dispenser = {
			override = true,
			condition = function(id, ply, ent)
				return file.Exists("models/doctorwho1200/sonics/12thdoctor/3rdpersonsonic.mdl", "GAME")
			end,
		},

		toyota_bookshelves = YES,
		toyota_lower_roundels = YES,
		toyota_throttle_sound_2017 = YES,
		toyota_demat_2014 = YES,
	},
})