local EXT_SCALE = 1.05
local EXT_SCALE_VECTOR = Vector(EXT_SCALE, EXT_SCALE, EXT_SCALE)

TARDIS:AddInteriorTemplate("toyota_f_box_base", {
	Exterior = {
		Mass=8000,
		Portal={
			pos=Vector(23,-0.9,49.1),
			ang=Angle(0,0,0),
			width=48,
			height=97,
		},
		Fallback={
			pos=Vector(50,0,10),
			ang=Angle(0,0,0)
		},
		Light={
			enabled=true,
			pos=Vector(0,0,121),
			color=Color(255,255,255),
		},
		Parts={
			door={
				posoffset=Vector(-23,0.9,-49.11),
				angoffset=Angle(0,0,0),
				matrixScale = EXT_SCALE_VECTOR,
			},
		},
		Sounds={
			Door={
				enabled=true,
				open="TARDISman/tardisman550/rewrite/tardis_2017_door_open_.wav",
				close="TARDISman/tardisman550/rewrite/tardis_2017_door_close_.wav",
			},
			Teleport={
				demat="TARDISman/tardisman550/rewrite/tardis_2017_demat_.wav",
				mat="TARDISman/tardisman550/rewrite/tardis_2017_mat_.wav",
				fullflight = "cem/toyota/fullext.wav",
			},
		},
		CustomHooks = {
			exterior_scale = {
				"PostInitialize",
				function(self)
					if CLIENT then
						local matrix = Matrix()
						matrix:Scale(EXT_SCALE_VECTOR)
						self:EnableMatrix("RenderMultiply", matrix)
					end
				end,
			},
		},
	},
	Interior = {
		Portal={
			pos=Vector(-335,0.9,135.7),
			ang=Angle(0,0,0),
			width=50,
			height=110,
		},
		Parts = {
			door = {
				posoffset=Vector(23,-0.9,-49.11),
				angoffset=Angle(0,180,0),
				matrixScale = EXT_SCALE_VECTOR,
			},
			toyota_doorframe = false,
		},
		TextureSets = {
			doorframe_on = {
				prefix = "models/TARDISman/tardisman550/",
				{ "door", 10, "light_f" },
				{ "door", 12, "topglass_f" },
				{ "door", 5, "metal_f" },
			},
			doorframe_off = {
				prefix = "models/TARDISman/tardisman550/",
				{ "door", 10, "light_f_off" },
				{ "door", 12, "topglass_f_off" },
				{ "door", 5, "metal_f_off" },
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_f_box_no_doorlight", {
	Interior = {
		TextureSets = {
			doorframe_on = {
				[1] = { [3] = "light_f_off", },
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_f_box_2014", {
	Exterior = {
		Model="models/TARDISman/tardisman550/rewrite/exterior_s8_f.mdl",
		Parts={
			door={ model="models/TARDISman/tardisman550/rewrite/doors_exterior_s8_f.mdl", },
		},
	},
	Interior = {
		Parts = {
			door = { model="models/TARDISman/tardisman550/rewrite/doors_interior_s8_f.mdl", },
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_f_box_2017", {
	Exterior = {
		Model="models/TARDISman/tardisman550/rewrite/exterior_f.mdl",
		Parts={
			door={ model="models/TARDISman/tardisman550/rewrite/doors_exterior_f.mdl", },
		},
	},
	Interior = {
		Parts = {
			door = { model="models/TARDISman/tardisman550/rewrite/doors_interior_f.mdl", },
		},
	},
})
