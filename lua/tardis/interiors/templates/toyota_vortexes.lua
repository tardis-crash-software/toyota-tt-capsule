TARDIS:AddInteriorTemplate("toyota_vortex_2013", {
	Exterior = {
		Parts = {
			vortex={
				model="models/doctorwho1200/toyota/2013timevortex.mdl",
				pos=Vector(0,0,0),
				ang=Angle(0,180,0),
				scale=10
			}
		},
	},
	Interior = {
		TextureSets = {
			screenvortex = {
				prefix = "models/doctorwho1200/toyota/",
				{ "toyota_monitor1", 3, "2013_vortex" },
				{ "toyota_monitor2", 3, "2013_vortex" },
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_vortex_2013B", {
	Exterior = {
		Parts = {
			vortex={
				model="models/doctorwho1200/toyota/2013timevortex.mdl",
				pos=Vector(0,0,0),
				ang=Angle(0,0,0),
				scale=10
			}
		},
	},
	Interior = {
		TextureSets = {
			screenvortex = {
				prefix = "models/doctorwho1200/toyota/",
				{ "toyota_monitor1", 3, "2013blue" },
				{ "toyota_monitor2", 3, "2013blue" },
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_vortex_2014", {
	Exterior = {
		Parts = {
			vortex={
				model="models/doctorwho1200/toyota/2014timevortex.mdl",
				pos=Vector(0,0,50),
				ang=Angle(0,0,0),
				scale=10
			}
		},
	},
	Interior = {
		TextureSets = {
			screenvortex = {
				prefix = "models/doctorwho1200/toyota/",
				{ "toyota_monitor1", 3, "2014vortex" },
				{ "toyota_monitor2", 3, "2014vortex" },
			},
		},
	},
})