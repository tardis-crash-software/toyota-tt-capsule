--------------------------------------------------------------------------------
-- Monitors
--------------------------------------------------------------------------------

TARDIS:AddInteriorTemplate("toyota_monitors_2012", {
	Interior = {
		Screens = {
			{
				pos = Vector(-27.486, -23.732, 165.399),
				ang = Angle(0, -30, 102),
				width = 379,
				height = 200,
			},
			{
				pos = Vector(27.486, 23.732, 165.399),
				ang = Angle(0, 150, 102),
				width = 379,
				height = 200,
			},
		},
		Parts={
			toyota_monitor1 = {
				model = "models/cem/toyota/controls/monitor_2012.mdl",
				ang = Angle(0,0,0),
			},
			toyota_monitor2 = {
				model = "models/cem/toyota/controls/monitor_2012.mdl",
				ang = Angle(0,180,0),
			},
			toyota_monitor_ring = { model = "models/cem/toyota/monitor_ring_2012.mdl", },
		},
		Controls = {
			toyota_monitor1 = false,
			toyota_button = "toggle_screens",
		},
		CustomHooks = {
			screens_condition = false,
		},
	},
	CustomHooks = {
		screens_sound = {
			inthooks = {
				["PostInitialize"] = true,
			},
			func = function(ext, int)
				local m1 = int:GetPart("toyota_monitor1")
				local m2 = int:GetPart("toyota_monitor2")
				if IsValid(m1) then m1.Sound = "" end
				if IsValid(m2) then m2.Sound = "" end
			end,
		},
		monitors_rotation = false,
	},
})

TARDIS:AddInteriorTemplate("toyota_monitors_2013", {
	Interior = {
		Parts = {
			toyota_monitor1 = {
				ang = Angle(0, 50, 0),
			},
			toyota_monitor2 = {},
			toyota_monitor_ring = {},
			toyota_monitor1_hitbox	= {
				pos = Vector(23.688, -38.182, 156.37),
				ang = Angle(2.694, 23.115, 97.364),
			},
		},
		Screens = {
			{
				pos = Vector(27.561, -38.1, 162.165),
				ang = Angle(0, 50, 96),
				width = 378,
				height = 200,
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_monitors_2015", {
	Interior = {
		Parts = {
			toyota_monitor1 = {
				model = "models/cem/toyota/controls/monitor_2015.mdl",
				ang = Angle(0, 100, 0)
			},
			toyota_monitor2 = {
				model = "models/cem/toyota/controls/monitor_2015.mdl",
			},
			toyota_monitor_ring = {},
			toyota_monitor1_hitbox	= {
				pos = Vector(44.475460, -6.396856, 156.369995),
				ang = Angle(2.694, 73.115, 97.364),
			},
		},
		Screens = {
			{
				pos = Vector(47.904316, -3.262608, 163.612000),
				ang = Angle(0, 100, 96),
				width = 378,
				height = 200,
			},
		},
	},
})

--------------------------------------------------------------------------------
-- Consoles
--------------------------------------------------------------------------------

TARDIS:AddInteriorTemplate("toyota_console_snowmen", {
	Interior = {
		Parts={
			toyota_console = { model = "models/cem/toyota/console_2012.mdl", },
			toyota_audio_system = { model = "models/cem/toyota/controls/audio_system_2012.mdl", },
			toyota_sonic_charger = false,
			toyota_upperconsolelights = false,
		},
		PartTips = {
			toyota_audio_system = false,
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_console_s7", {
	Interior = {
		Parts={
			toyota_console = { model = "models/cem/toyota/console_2012.mdl", },
			toyota_audio_system = { model = "models/cem/toyota/controls/audio_system_2012.mdl", },
			toyota_sonic_charger = false,
			toyota_upperconsolelights = { ang = Angle(0, 90, 0), },
		},
		PartTips = {
			toyota_audio_system = false,
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_console_dotd_holder_empty", {
	Interior = {
		Parts={
			toyota_holder = {ang = Angle(0,90,0)},
			toyota_sonic_charger = false,
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_console_11th_sonic_dispenser", {
	Interior = {
		Parts = {
			toyota_sonic = {
				pos = Vector(4.22, -34.015, 136.396),
				ang = Angle(15.301, -38.846, 10.003),
			},
		},
		Controls = {
			toyota_sonic = "toyota_sonic_dispenser",
			toyota_sonic_charger = "toyota_sonic_dispenser",
		},
	},
	ToyotaSonicID = "11thdoctorsonic",
})

TARDIS:AddInteriorTemplate("toyota_console_12th_sonic_dispenser", {
	Interior = {
		Parts = {
			toyota_sonic = {
				model = "models/doctorwho1200/sonics/12thdoctor/3rdpersonsonic.mdl",
				pos = Vector(11.144, -34.066, 137.488),
				ang = Angle(-5.702, -143.291, -17.723),
			},
		},
		Controls = {
			toyota_sonic = "toyota_sonic_dispenser",
			toyota_sonic_charger = "toyota_sonic_dispenser",
		},
	},
	ToyotaSonicID = "12thdoctorsonic",
})

--------------------------------------------------------------------------------
-- Floor types
--------------------------------------------------------------------------------

TARDIS:AddInteriorTemplate("toyota_floor_s7", {
	Interior = {
		Parts={
			toyota_floor = { model = "models/cem/toyota/floor_2012.mdl", },
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_floor_totd", {
	Interior = {
		Parts={
			toyota_floor = {},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_floor_dotd_step", {
	Interior = {
		Parts={
			toyota_floor = {},
			toyota_lights_floor = { model = "models/cem/toyota/lights_floor_dotd_step.mdl",},
			toyota_floor_dotd_step = {},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_floor_2014", {
	Interior = {
		Model="models/cem/toyota/interior_2014.mdl",

		Parts={
			toyota_rails = { model = "models/cem/toyota/rails_capaldi.mdl", },
			toyota_floor = { model = "models/cem/toyota/floor_2014.mdl", },
			toyota_lower_trim = { model = "models/cem/toyota/lower_trim_capaldi.mdl", },
			toyota_lights_catwalk_lower		= {pos = Vector(0, 0, 0), ang = Angle(0,90,0),},
		},
	},
})

--------------------------------------------------------------------------------
-- Throttle sounds
--------------------------------------------------------------------------------

TARDIS:AddInteriorTemplate("toyota_throttle_sound_2014", {
	CustomHooks = {
		throttle_sound = {
			inthooks = {
				["PostInitialize"] = true,
			},
			func = function(ext, int)
				local t = int:GetPart("toyota_throttle")
				if IsValid(t) then t.Sound = "cem/toyota/throttle2.wav" end
			end,
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_throttle_sound_2015", {
	CustomHooks = {
		throttle_sound = {
			inthooks = {
				["PostInitialize"] = true,
			},
			func = function(ext, int)
				local t = int:GetPart("toyota_throttle")
				if IsValid(t) then t.Sound = "Poogie/toyota/throttle_2015.wav" end
			end,
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_throttle_sound_2017", {
	CustomHooks = {
		throttle_sound = {
			inthooks = {
				["PostInitialize"] = true,
			},
			func = function(ext, int)
				local t = int:GetPart("toyota_throttle")
				if IsValid(t) then t.Sound = "Poogie/toyota/thr0ttle_2017.wav" end
			end,
		},
	},
})

--------------------------------------------------------------------------------
-- Separate features
--------------------------------------------------------------------------------

TARDIS:AddInteriorTemplate("toyota_totd_green_rotor", {
	CustomHooks = {
		throttle_sound = {
			inthooks = {
				["PostInitialize"] = true,
			},
			func = function(ext, int)
				local glass = int:GetPart("toyota_glass")
				if IsValid(glass) then
					glass:SetBodygroup(1,1)
					return
				end
			end,
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_bookshelves", {
	Interior = {
		Parts = {
			toyota_books = {},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_phone_port", {
	Interior = {
		Parts = {
			toyota_phone_port = { pos = Vector(-40.902, -14.452, 134.959), ang = Angle(0,90,0), },
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_lower_roundels", {
	Interior = {
		Parts={
			toyota_walls_lower = false,
			toyota_walls_lower_roundels = { ang = Angle(0,90,0), },
			toyota_lights_lower_roundels = { ang = Angle(0,90,0), },
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_stasis_cube", {
	Interior = {
		Parts={
			toyota_stasiscube = {
				pos = Vector(23.116, -29.103, 87.753),
				ang = Angle(0, 72.49, 0),
			},
		},
	},
})


--------------------------------------------------------------------------------
--DEMAT sounds--
--------------------------------------------------------------------------------

TARDIS:AddInteriorTemplate("toyota_demat_2017", {
	Interior = {
		Sounds={
			Teleport={
				demat="Poogie/toyota/demat_2017.wav",
				fullflight = "Poogie/toyota/full_2017_TUAT.wav",

			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_demat_2015", {
	Interior = {
		Sounds={
			Teleport={
				demat="Poogie/toyota/demat_2o15_S9.wav",
				fullflight = "Poogie/toyota/full_2o14.wav",

			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_demat_2012_snowmen", {
	Interior = {
		Sounds={
			Teleport={
				demat="Poogie/toyota/demat_2013_snowmen.wav",
				fullflight = "Poogie/toyota/fullflight_2012_.wav",

			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_demat_2014", {
	Interior = {
		Sounds={
			Teleport={
				demat="Poogie/toyota/demat_2o14.wav",
				fullflight = "Poogie/toyota/full_2o14.wav",

			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_demat_2013_notd", {
	Interior = {
		Sounds={
			Teleport={
				demat="Poogie/toyota/demat_notd_.wav",
				fullflight = "Poogie/toyota/full_2013.wav",

			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_demat_2013_dotd", {
	Interior = {
		Sounds={
			Teleport={
				demat="Poogie/toyota/demat_notd_.wav",
				fullflight = "Poogie/toyota/full_2013.wav",
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_demat_2013_totd", {
	Interior = {
		Sounds={
			Teleport={
				demat="Poogie/toyota/demat_notd_.wav",
				fullflight = "Poogie/toyota/full_2013.wav",

			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_demat_2017_F", {
	Interior = {
		Sounds={
			Teleport={
				demat="Poogie/toyota/demat_2015_S9.wav",
				fullflight = "Poogie/toyota/full_2o14.wav",

			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_demat_stock", {
	Interior = {
		Sounds={
			Teleport={
				demat="cem/toyota/demat.wav"

			},
		},
	},
})


------------------------------------------------------
--Mat sounds--
------------------------------------------------------

TARDIS:AddInteriorTemplate("toyota_mat_2012_snowmen", {
	Interior = {
		Sounds={
			Teleport={
				mat="Poogie/toyota/mat_snowmen.wav"
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_mat_2017_TUAT", {
	Interior = {
		Sounds={
			Teleport={
				mat="Poogie/toyota/mat_2017_TUAT.wav"
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_mat_S7", {
	Interior = {
		Sounds={
			Teleport={
				mat="Poogie/toyota/mat_notd.wav"
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_mat_2014_stock", {
	Interior = {
		Sounds={
			Teleport={
				mat="cem/toyota/mat.wav"
			},
		},
	},
})

-----------------------
--Misc sounds
-----------------------
TARDIS:AddInteriorTemplate("toyota_demat_damaged_HORS", {
	Interior = {
		Sounds={
			Teleport={
				mat_damaged = "Poogie/toyota/mat_damaged_HORS.wav",
				demat_damaged = "Poogie/toyota/demat_damaged_HORS.wav"
			},
		},
	},
})

