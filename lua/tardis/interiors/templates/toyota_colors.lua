TARDIS:AddInteriorTemplate("toyota_interior_blue", {
	Interior = {
		Light={
			color=Color(0,255,215),
			warncolor=Color(0,51,51),
		},
		Lights={
			console_white = {
				color=Color(255,255,255),
				warncolor=Color(255,143,143),
			},
			lower_light = {
				color=Color(0,255,215),
				warncolor=Color(51,102,102),
			},
		},
		TextureSets = {
			initial_roundel_trim = {
				prefix = "models/cem/toyota_smith/",
			},
			poweron_specific = {
				prefix = "models/cem/toyota_smith/",
			},
			poweroff_specific = {
				base = "poweron_specific",
				prefix = "models/cem/toyota_smith/off/",
			},
			screenon = {
				prefix = "models/cem/toyota_smith/",
			},
			-- screenvortex is defined in vortex template
			screenoff = {
				prefix = "models/cem/toyota_smith/",
			},
			screenwarning = {
				prefix = "models/cem/toyota_smith/",
			},
		},
	},
	ToyotaEasterEggSound = "cem/toyota/clara_smith.wav",
})

TARDIS:AddInteriorTemplate("toyota_interior_orange", {
	Interior = {
		Light={
			color=Color(255,85,0),
			warncolor=Color(255,83,52),
		},
		Lights={
			console_white = {
				color=Color(255,255,255),
				warncolor=Color(255,81,0),
			},
			lower_light = {
				color=Color(255,85,0),
				warncolor=Color(255,83,52),
			},
		},
		TextureSets = {
			initial_roundel_trim = {
				prefix = "models/cem/toyota_capaldi/",
			},
			poweron_specific = {
				prefix = "models/cem/toyota_capaldi/",
			},
			poweroff_specific = {
				base = "poweron_specific",
				prefix = "models/cem/toyota_capaldi/off/",
			},
			screenon = {
				prefix = "models/cem/toyota_capaldi/",
			},
			-- screenvortex is defined in vortex template
			screenoff = {
				prefix = "models/cem/toyota_capaldi/",
			},
			screenwarning = {
				prefix = "models/cem/toyota_capaldi/",
			},
		},
	},
	ToyotaEasterEggSound = "cem/toyota/clara_capaldi.wav",
})

TARDIS:AddInteriorTemplate("toyota_interior_orange_bright", {
	Interior = {
		LightOverride = {
			basebrightness = 0.04,
			nopowerbrightness = 0.025,
		},
		Light={
			color=Color(255,85,0),
			warncolor=Color(255,40,0),
			brightness=0.7,
		},
		Lights={
			console_white = {
				color=Color(163,208,245),
				warncolor=Color(255,110,110),
				pos=Vector(0,0,189.5),
				brightness=1.5
			},
			lower_light = {
				color=Color(255,85,0),
				warncolor=Color(255,40,0),
				brightness=0.5
			},
		},
		TextureSets = {
			initial_roundel_trim = {
				prefix = "models/cem/toyota_capaldi/",
			},
			poweron_specific = {
				prefix = "models/cem/toyota_capaldi/",
			},
			poweroff_specific = {
				base = "poweron_specific",
				prefix = "models/cem/toyota_capaldi/off/",
			},
			screenon = {
				prefix = "models/cem/toyota_capaldi/",
			},
			-- screenvortex is defined in vortex template
			screenoff = {
				prefix = "models/cem/toyota_capaldi/",
			},
			screenwarning = {
				prefix = "models/cem/toyota_capaldi/",
			},
		},
	},
	ToyotaEasterEggSound = "cem/toyota/clara_capaldi.wav",
})

TARDIS:AddInteriorTemplate("toyota_interior_blue_bright", {
	Interior = {
		LightOverride = {
			basebrightness = 0.055,
			nopowerbrightness = 0.025,
		},
		Light={
			color=Color(0,255,215),
			warncolor=Color(0,51,51),
			brightness=0.7,
		},
		Lights={
			console_white = {
				color=Color(254,253,241),
				warncolor=Color(255,110,110),
				pos=Vector(0,0,189.5),
				brightness=1.5,
			},
			lower_light = {
				color=Color(0,255,215),
				warncolor=Color(51,102,102),
				brightness=0.5,
			},
		},
		TextureSets = {
			initial_roundel_trim = {
				prefix = "models/cem/toyota_smith/",
			},
			poweron_specific = {
				prefix = "models/cem/toyota_smith/",
			},
			poweroff_specific = {
				base = "poweron_specific",
				prefix = "models/cem/toyota_smith/off/",
			},
			screenon = {
				prefix = "models/cem/toyota_smith/",
			},
			-- screenvortex is defined in vortex template
			screenoff = {
				prefix = "models/cem/toyota_smith/",
			},
			screenwarning = {
				prefix = "models/cem/toyota_smith/",
			},
		},
	},
	ToyotaEasterEggSound = "cem/toyota/clara_smith.wav",
})