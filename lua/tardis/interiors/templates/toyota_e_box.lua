TARDIS:AddInteriorTemplate("toyota_e_box_base", {
	Exterior = {
		Mass=2900,
		Portal={
			pos=Vector(27.8,0,52.14),
			ang=Angle(0,0,0),
			width=50,
			height=92
		},
		Fallback={
			pos=Vector(55,0,5),
			ang=Angle(0,0,0)
		},
		Light={
			enabled=true,
			pos=Vector(0,0,122),
			color=Color(255,228,91)
		},
		Parts={
			door={
				posoffset=Vector(-1,0,-51.9),
				angoffset=Angle(0,0,0)
			},
		},
		Sounds={
			Door={
				enabled=true,
				open="Poogie/toyota/dooropened.wav",
				close="Poogie/toyota/doorclosed.wav",
			},
			Teleport={
				demat="Poogie/toyota/dematext_E.wav",
				mat="Poogie/toyota/matext_E_box.wav",
				fullflight = "cem/toyota/fullext.wav",
			},
		},
	},
	Interior = {
		Portal={
			pos=Vector(-335,0,139),
			ang=Angle(0,0,0),
			width=50,
			height=92,
		},
		Parts = {
			door = {
				posoffset=Vector(0.8,0,-51.9),
				angoffset=Angle(0,180,0),
			},
			toyota_doorframe = {
				pos = Vector(3.5, 0, 5.9),
				ang = Angle(0,90,0),
				matrixScale = Vector(0.99, 1, 1.02),
			},
		},
		TextureSets = {
			doorframe_on = {
				prefix = "models/doctorwho1200/toyota/2013/",
				{ "door", 6, "lock" },
			},
			doorframe_off = {
				prefix = "models/doctorwho1200/toyota/2013/",
				{ "door", 6, "lock_off" },
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_e_box_2013", {
	Exterior = {
		Model="models/doctorwho1200/toyota/exterior_2013.mdl",
		Parts={
			door={ model="models/doctorwho1200/toyota/doorsext_2013.mdl", },
		},
	},
	Interior = {
		Parts = {
			door = { model="models/doctorwho1200/toyota/doors_2013.mdl",},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_e_box_2017", {
	Exterior = {
		Model="models/doctorwho1200/toyota/exterior_2017.mdl",
		Parts={
			door={ model="models/doctorwho1200/toyota/doorsext_2017.mdl",},
		},
	},
	Interior = {
		Parts = {
			door = { model="models/doctorwho1200/toyota/doors_2017.mdl",},
		},
	},
})
