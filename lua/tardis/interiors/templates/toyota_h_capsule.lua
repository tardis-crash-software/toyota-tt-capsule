TARDIS:AddInteriorTemplate("toyota_h_capsule_base", {
	Exterior = {
		Mass=2900,
		Portal={
			pos=Vector(13.76,0,52.22),
			ang=Angle(0,0,0),
			width=35,
			height=88
		},
		Fallback={
			pos=Vector(55,0,5),
			ang=Angle(0,0,0)
		},
		Light={
			enabled=false,
			pos=Vector(0,0,122),
			color=Color(255,228,91)
		},
		Parts={
			door={
				posoffset=Vector(0,0,0),
				angoffset=Angle(0,0,0)
			},
		},
		Sounds={
			Door={
				enabled=true,
				open="TARDISman/tardisman550/rewrite/doorext_open.wav",
				close="TARDISman/tardisman550/rewrite/doorext_close.wav",
			},
			Teleport={
				demat="cem/toyota/dematext.wav",
				mat="cem/toyota/matext.wav",
				fullflight = "cem/toyota/fullext.wav",
			},
		},
	},
	Interior = {
		Portal={
			pos=Vector(-335,0,139),
			ang=Angle(0,0,0),
			width=26,
			height=92,
		},
		Parts = {
			door = {
				posoffset=Vector(0,0,0),
				angoffset=Angle(0,180,0),
			},
			toyota_doorframe = {
				pos = Vector(35, 0, 200),
				ang = Angle(0,90,0),
				matrixScale = Vector(0.99, 1, 1.02),
			},
		},
		TextureSets = {
			doorframe_on = {
				prefix = "models/doctorwho1200/toyota/2013/",
				{ "door", 6, "lock" },
			},
			doorframe_off = {
				prefix = "models/doctorwho1200/toyota/2013/",
				{ "door", 6, "lock_off" },
			},
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_h_capsule_2013", {
	Exterior = {
		Model="models/toast/TTCapsule/exterior.mdl",
		Parts={
			door={ model="models/toast/ttcapsule/doorsext.mdl", },
		},
	},
	Interior = {
		Parts = {
			door = { model="models/toast/ttcapsule/doors.mdl",},
		},
	},
})
